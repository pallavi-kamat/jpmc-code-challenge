//
//  UIConstants.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import Foundation

enum StringConstants {
    static let EmptyString = ""
    static let EmptyBlankString = " "
    static let ErrorTitle = "Error"
    static let ButtonTitle_OK = "OK"
    static let mathScore = "Math : "
    static let readingScore = "Reading : "
    static let writingScore = "Writing : "
    static let internetConnectionMonitor = "InternetConnectionMonitor"
    static let noInternetMsg = "Please check your internet connection."
}

struct CellReuseIdentifier {
    static let dashboardReuseIdentifier = "CollectionViewCell"
}

struct Storyboards {
    static let main = "Main"
}

struct StoryboardIdentifiers {
    static let viewController = "ViewController"
    static let detailViewController = "DetailViewController"
}
