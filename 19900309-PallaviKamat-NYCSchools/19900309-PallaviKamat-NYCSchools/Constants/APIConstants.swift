//
//  APIConstants.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit

class APIConstants {
    static let baseURL = "https://data.cityofnewyork.us/resource"
    
    struct EndPoints {
        static let schoolList = "/s3k6-pzi2.json"
        static let schoolDetail = "/f9bf-2cp4.json?dbn="
    }
    
    struct RequestType {
        static let Post = "POST"
        static let Get = "GET"
        static let Put = "PUT"
        static let Delete = "DELETE"
    }
    
    struct Response {
        static let status = "status"
        static let statusCode = "statusCode"
        static let success = "success"
        static let failure = "failure"
        static let error = "error"
        static let errorNumber = "errorNumber"
        static let message = "message"
        static let invalidState = "invalid state"
        static let cancelComplete = "Cancel Completed!"
        static let exists = "exists"
        static let restricted = "restricted"
    }
    
    struct ResponseStatusCode {
        static let TwoHundered = 200 //Success
        static let FourZeroThree = 403 // Forbidden
        static let FourZeroOne = 401 //unauthorised
        static let OneThousand = 1000
    }
}
