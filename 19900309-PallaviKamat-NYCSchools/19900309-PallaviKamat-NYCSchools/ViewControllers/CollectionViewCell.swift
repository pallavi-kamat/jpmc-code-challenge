//
//  CollectionViewCell.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/14/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lblLocation.sizeToFit()
        // Initialization code
    }
    
    func configure(schoolData:SchoolData) {
        lblSchoolName.text = schoolData.school_name
        lblEmail.text = schoolData.school_email
        lblLocation.text = schoolData.primary_address_line_1!+", "+schoolData.city!
    }
}
