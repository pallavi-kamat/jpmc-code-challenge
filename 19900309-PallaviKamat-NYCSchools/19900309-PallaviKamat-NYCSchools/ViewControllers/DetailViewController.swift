//
//  detailViewController.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/13/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    // MARK: - Properties
    fileprivate var detailViewModel = DetailViewModel()
    var schoolData:SchoolData = SchoolData()
    let regionRadius: CLLocationDistance = 1000
    
    @IBOutlet weak var mapSchool: MKMapView!
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblMathScore: UILabel!
    @IBOutlet weak var lblReadingScore: UILabel!
    @IBOutlet weak var lblWritingScore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        getSchoolDetailData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func initialSetUp() {
        lblSchoolName.sizeToFit()
        // set initial location in Honolulu
        let initialLocation = CLLocation(latitude: (schoolData.latitude! as NSString).doubleValue, longitude: (schoolData.longitude! as NSString).doubleValue)
        centerMapOnLocation(location: initialLocation)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapSchool.setRegion(coordinateRegion, animated: true)
        // show artwork on map
        let artwork = Artwork(title: schoolData.school_name!,
                              locationName: schoolData.location!,
                              discipline: "",
                              coordinate: CLLocationCoordinate2D(latitude: (schoolData.latitude! as NSString).doubleValue, longitude: (schoolData.latitude! as NSString).doubleValue))
        mapSchool.addAnnotation(artwork)
    }
    
    // ViewModel call for fetching the School results
    ///
    /// - Parameters:
    func getSchoolDetailData() {
        detailViewModel.fetchSchoolDetails(strSchoolId: schoolData.dbn!, completionHandler: { (response,errorCode) in
            DispatchQueue.main.async {
            }
            switch(errorCode) {
            case APIConstants.ResponseStatusCode.TwoHundered:
                DispatchQueue.main.async {
                if(self.detailViewModel.schoolDetailData?.count ?? 0>0) {
                    let schoolData = self.detailViewModel.schoolDetailData?[0]
                    self.lblSchoolName.text = schoolData?.school_name
                    self.lblMathScore.text = StringConstants.mathScore + (schoolData?.sat_math_avg_score)!
                    self.lblReadingScore.text = StringConstants.readingScore +  (schoolData?.sat_critical_reading_avg_score)!
                    self.lblWritingScore.text = StringConstants.mathScore + (schoolData?.sat_writing_avg_score)!
                    self.navigationController?.navigationItem.title = schoolData?.school_name
                }
                }
            case APIConstants.ResponseStatusCode.FourZeroOne,
                 APIConstants.ResponseStatusCode.FourZeroThree,
                 APIConstants.ResponseStatusCode.OneThousand:
                DispatchQueue.main.async {
                }
            default:
                DispatchQueue.main.async {
                  
                }
            }
            
        })
    }
}
