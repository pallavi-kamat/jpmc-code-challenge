//
//  ViewController.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/13/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit
import Network

class ViewController: UIViewController{
   // let reuseIdentifier = "CollectionViewCell"
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: StringConstants.internetConnectionMonitor)
    // MARK: - Properties
    fileprivate var dashbaordViewModel = DashbaordViewModel()
    
    @IBOutlet weak var collectionViewSchools: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNYSchoolData()
        monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                //Internet connection is on
            } else {
                Utility.showAlert(strAlertText: StringConstants.noInternetMsg, controller: self)
            }
        }
        monitor.start(queue: queue)
    }
    
    // ViewModel call for fetching the School results
    ///
    /// - Parameters:
    func getNYSchoolData() {
        dashbaordViewModel.fetchSchoolResults(completionHandler: { (response,errorCode) in
            DispatchQueue.main.async {
            }
            switch(errorCode) {
            case APIConstants.ResponseStatusCode.TwoHundered:
                DispatchQueue.main.async {
                    self.collectionViewSchools.reloadData()
                }
            case APIConstants.ResponseStatusCode.FourZeroOne,
                 APIConstants.ResponseStatusCode.FourZeroThree,
                 APIConstants.ResponseStatusCode.OneThousand:
                DispatchQueue.main.async {
                }
            default:
                DispatchQueue.main.async {
                    self.collectionViewSchools.reloadData()
                }
            }
            
        })
    }
}

extension ViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dashbaordViewModel.schoolData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellReuseIdentifier.dashboardReuseIdentifier, for: indexPath) as? CollectionViewCell {
            // Configure the cell
            cell.configure(schoolData: dashbaordViewModel.schoolData?[indexPath.row] ?? SchoolData())
            
            return cell
        }
        
        return CollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let schoolData = dashbaordViewModel.schoolData?[indexPath.row] ?? SchoolData()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: Storyboards.main, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: StoryboardIdentifiers.detailViewController) as! DetailViewController
        nextViewController.schoolData = schoolData
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
}

