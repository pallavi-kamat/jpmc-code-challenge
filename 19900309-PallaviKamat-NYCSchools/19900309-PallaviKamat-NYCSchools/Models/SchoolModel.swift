//
//  SchoolModel.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import Foundation

struct SchoolData: Codable {
    var school_name: String?
    var school_email: String?
    var primary_address_line_1: String?
    var city: String?
    var dbn: String?
    var latitude: String?
    var longitude: String?
    var location: String?
}
