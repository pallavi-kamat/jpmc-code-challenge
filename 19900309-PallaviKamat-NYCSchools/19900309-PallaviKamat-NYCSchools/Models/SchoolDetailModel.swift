//
//  SchoolDetailModel.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import Foundation

struct SchoolDetailData: Codable {
    var school_name:String?
    var sat_critical_reading_avg_score:String?
    var sat_math_avg_score:String?
    var sat_writing_avg_score:String?
}
