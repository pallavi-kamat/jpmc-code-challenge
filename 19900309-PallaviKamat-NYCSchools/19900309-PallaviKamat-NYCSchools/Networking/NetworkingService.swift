//
//  NetworkingService.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import Foundation

class NetworkingService: NSObject {
    // URLSession Network call
    ///
    /// - Parameters:
    ///   - urlString: String
    ///   - requestType: String
    ///   - requestType: Dictionary
    ///   - completionHandler: It returns the response to caller in specified return type
    
    class func callWebservice(with urlString: String, requestType: String, dictionary: [String: Any]?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        if let url = URL(string: urlString) {
            let session = URLSession.shared
            session.getTasksWithCompletionHandler { (dataTasks, uploadTasks, downloadTasks) in
                let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval:30)
                
                request.httpMethod = requestType
                if let dictionary = dictionary {
                    request.httpBody = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
                }
                
                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                    if (error != nil) {
                        completionHandler(nil, nil, error)
                    } else {
                        if let data = data {
                            completionHandler(data, response, nil)
                        } else {
                            completionHandler(nil, response, error)
                        }
                    }
                })
                dataTask.resume()
            }
        } else {
            // error hanling
        }
    }
    
}
