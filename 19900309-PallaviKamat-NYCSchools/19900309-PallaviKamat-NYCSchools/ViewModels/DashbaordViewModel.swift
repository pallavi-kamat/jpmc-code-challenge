//
//  DashbaordViewModel.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit

class DashbaordViewModel: NSObject {
    var schoolData : [SchoolData]?
    
    // WebService call for fetching the NY School list
    ///
    /// - Parameters:
    ///   - completionHandler: It returns the response to caller in specified return type
    func fetchSchoolResults(completionHandler: @escaping (Data?, Int?) -> Void) {
       // clearResults()
        let urlString = APIConstants.baseURL + APIConstants.EndPoints.schoolList
        NetworkingService.callWebservice(with: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? StringConstants.EmptyString, requestType: APIConstants.RequestType.Get, dictionary: nil) { (jsonData, response, error) in
            let httpResponse = response as? HTTPURLResponse
            if error == nil,
                let jsonData = jsonData {
                guard let schoolData = try? JSONDecoder().decode([SchoolData].self, from: jsonData) else {
                    completionHandler(jsonData,APIConstants.ResponseStatusCode.OneThousand)
                    print("Error: Couldn't decode data into Blog")
                    return
                }
                self.schoolData = schoolData
               // print(schoolData)
                completionHandler(jsonData,httpResponse?.statusCode)
            }
            else if httpResponse?.statusCode == APIConstants.ResponseStatusCode.FourZeroOne {
                // error handling
                completionHandler(nil,APIConstants.ResponseStatusCode.FourZeroOne)
            }
            else {
                // error handling
            }
        }
    }
}
