//
//  DetailViewModel.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/18/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit

class DetailViewModel: NSObject {
    var schoolDetailData : [SchoolDetailData]?
    
    // WebService call for fetching the NY School list
    ///
    /// - Parameters:
    ///   - completionHandler: It returns the response to caller in specified return type
    func fetchSchoolDetails(strSchoolId:String, completionHandler: @escaping (Data?, Int?) -> Void) {
        // clearResults()
        let urlString = APIConstants.baseURL + APIConstants.EndPoints.schoolDetail+strSchoolId
        NetworkingService.callWebservice(with: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? StringConstants.EmptyString, requestType: APIConstants.RequestType.Get, dictionary: nil) { (jsonData, response, error) in
            let httpResponse = response as? HTTPURLResponse
            if error == nil,
                let jsonData = jsonData {
                
                guard let schoolDetailData = try? JSONDecoder().decode([SchoolDetailData].self, from: jsonData) else {
                completionHandler(jsonData,APIConstants.ResponseStatusCode.OneThousand)
                    print("Error: Couldn't decode data into Blog")
                    return
                }
                self.schoolDetailData = schoolDetailData
                print(schoolDetailData)
                completionHandler(jsonData,httpResponse?.statusCode)
            }
            else if httpResponse?.statusCode == APIConstants.ResponseStatusCode.FourZeroOne {
                // error handling
                completionHandler(nil,APIConstants.ResponseStatusCode.FourZeroOne)
            }
            else {
                // error handling
            }
        }
    }
}
