//
//  Utility.swift
//  19900309-PallaviKamat-NYCSchools
//
//  Created by Pallavi Kamat on 3/19/20.
//  Copyright © 2020 JPMC-Test. All rights reserved.
//

import UIKit

struct Utility {
    static func showAlert(strAlertText:String, controller:UIViewController) {
        let alertController = UIAlertController(title: nil, message: strAlertText, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: StringConstants.ButtonTitle_OK, style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        controller.present(alertController, animated: true, completion: nil)
    }
}
